var express = require("express");
var router = express.Router();
var models = require("../models");

router.get("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.alumno
    .findAll({
      attributes: ["id", "nombre", "direccion", "telefono", "mail"],
      include:[{as:'Carrera-Relacionada', model:models.carrera, attributes: ["id", "nombre"]}],
      offset: Number(req.query.offset),
      limit: Number(req.query.limit)
    })
    .then(alumnos => res.send(alumnos))
    .catch(() => res.sendStatus(500));
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.post("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.alumno
    .create({ nombre: req.body.nombre, direccion: req.body.direccion, telefono: req.body.telefono,
    mail: req.body.mail, id_carrera: req.body.id_carrera })
    .then(alumno => res.status(201).send({ id: alumno.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

const findAlumno = (id, { onSuccess, onNotFound, onError }) => {
  
  models.alumno
    .findOne({
      attributes: ["id", "nombre", "direccion", "telefono", "mail"],
      include:[{as:'Carrera-Relacionada', model:models.carrera, attributes: ["id", "nombre"]}],
      where: { id }
    })
    .then(alumno => (alumno ? onSuccess(alumno) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        findAlumno(req.params.id, {
          onSuccess: alumno => res.send(alumno),
          onNotFound: () => res.sendStatus(404),
          onError: () => res.sendStatus(500)
        });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.put("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = alumno =>
        alumno
          .update({ nombre: req.body.nombre, direccion: req.body.direccion, telefono: req.body.telefono,
            mail: req.body.mail, id_carrera: req.body.id_carrera }, { fields: ["nombre", "direccion", "telefono", "mail", "id_carrera"] })
          .then(() => res.sendStatus(200))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
        findAlumno(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.delete("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = carrera =>
        carrera
          .destroy()
          .then(() => res.sendStatus(200))
          .catch(() => res.sendStatus(500));
      findCarrera(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

module.exports = router;
