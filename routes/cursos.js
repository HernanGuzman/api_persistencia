var express = require("express");
var router = express.Router();
var models = require("../models");

router.get("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.curso
    .findAll({
      attributes: ["id", "horario"],
      include:[{as:'Materia-Relacionada', model:models.materia, attributes: ["id", "nombre"]},
      {as:'Profesor-Relacionado', model:models.profesor, attributes: ["id", "nombre", "direccion", "telefono", "mail"]}],
      offset: Number(req.query.offset),
      limit: Number(req.query.limit)
    })
    .then(cursos => res.send(cursos))
    .catch(() => res.sendStatus(500));
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.post("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.curso
    .create({ horario: req.body.horario, id_materia: req.body.id_materia, id_profesor: req.body.id_profesor })
    .then(curso => res.status(201).send({ id: curso.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: error inesperado')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

const findCurso = (id, { onSuccess, onNotFound, onError }) => {
  
  models.curso
    .findOne({
      attributes: ["id", "horario"],
      include:[{as:'Materia-Relacionada', model:models.materia, attributes: ["id", "nombre"]},
      {as:'Profesor-Relacionado', model:models.profesor, attributes: ["id", "nombre", "direccion", "telefono", "mail"]}],
      where: { id }
    })
    .then(curso => (curso ? onSuccess(curso) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      
      if(user){
        
        findCurso(req.params.id, {
          onSuccess: curso => res.send(curso),
          onNotFound: () => res.sendStatus(404),
          onError: () => res.sendStatus(500)
        });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.put("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = curso =>
        curso
          .update({ nombre: req.body.nombre, direccion: req.body.direccion, telefono: req.body.telefono,
            mail: req.body.mail, id_materia: req.body.id_materia, id_profesor: req.body.id_profesor }, { fields: ["horario", "id_materia", "id_profesor"] })
          .then(() => res.sendStatus(200))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: error inesperado')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
        findCurso(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.delete("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = curso =>
        curso
          .destroy()
          .then(() => res.sendStatus(200))
          .catch(() => res.sendStatus(500));
      findCurso(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

module.exports = router;