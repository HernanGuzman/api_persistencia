var express = require("express");
var router = express.Router();
var models = require("../models");
const bcrypt = require("bcrypt");


router.get("/", (req, res) => {
  models.user
    .findAll({
      attributes: ["id", "nombre", "email", "pass"],
      offset: Number(req.query.offset),
      limit: Number(req.query.limit)
      
    })
    .then(usuarios => res.send(usuarios))
    .catch(() => res.sendStatus(500));
});

router.post("/register", (req, res) => {
    
    const hash = bcrypt.hashSync(req.body.pass, 10);
    
    models.user
    .create({ nombre: req.body.nombre, email: req.body.email, pass: hash})
      .then(user => res.status(201).send({ id: user.id }))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otro usuario con el mismo email')
        }
        else {
          console.log(`Error al intentar insertar en la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
  });

/* Register Route
========================================================= 
router.post('/register', async (req, res) => {
    console.log("Llego al register");
    console.log(req.body.pass);
    // hash the password provided by the user with bcrypt so that
    // we are never storing plain text passwords. This is crucial
    // for keeping your db clean of sensitive data
    const hash = bcrypt.hashSync(req.body.pass, 10);
    console.log(hash);
    try {
      // create a new user with the password hash from bcrypt
      let user = await user
      .create({ nombre: req.body.nombre, email: req.body.email, pass: hash})
      .then(user => res.status(201).send({ id: user.id }))
      ;
      
  
      
  
    } catch(err) {
      return res.status(400).send(err);
    }
  
  });
*/

 /* Login Route
========================================================= */
router.post('/login', async (req, res) => {
    const { email, pass } = req.body;
  
    // if the username / password is missing, we use status code 400
    // indicating a bad request was made and send back a message
    if (!email || !pass) {
      return res.status(400).send(
        'Request missing email or pass param'
      );
    }
  
    try {
      let user = await user.authenticate(email, pass)
  
      user = await user.authorize();
  
      return res.json(user);
  
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
  });

/* Logout Route
========================================================= */
router.delete('/logout', async (req, res) => {

    // because the logout request needs to be send with
    // authorization we should have access to the user
    // on the req object, so we will try to find it and
    // call the model method logout
    const { user, cookies: { auth_token: authToken } } = req
  
    // we only want to attempt a logout if the user is
    // present in the req object, meaning it already
    // passed the authentication middleware. There is no reason
    // the authToken should be missing at this point, check anyway
    if (user && authToken) {
      await req.user.logout(authToken);
      return res.status(204).send()
    }
  
    // if the user missing, the user is not logged in, hence we
    // use status code 400 indicating a bad request was made
    // and send back a message
    return res.status(400).send(
      { errors: [{ message: 'not authenticated' }] }
    );
  });

const findUser = (id, { onSuccess, onNotFound, onError }) => {
  models.user
    .findOne({
      attributes: ["id", "nombre", "email", "pass"],
      where: { id }
    })
    .then(user => (user ? onSuccess(user) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", (req, res) => {
  findUser(req.params.id, {
    onSuccess: user => res.send(user),
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

router.put("/:id", (req, res) => {
  const onSuccess = user =>
    user
      .update({ nombre: req.body.nombre, email:req.body.email, pass:req.body.pass }, { fields: ["nombre", "email", "pass"] })
      .then(() => res.sendStatus(200))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otra materia con el mismo nombre')
        }
        else {
          console.log(`Error al intentar actualizar la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
    findUser(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

router.delete("/:id", (req, res) => {
  const onSuccess = user =>
  user
      .destroy()
      .then(() => res.sendStatus(200))
      .catch(() => res.sendStatus(500));
  findUser(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

module.exports = router;