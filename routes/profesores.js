var express = require("express");
var router = express.Router();
var models = require("../models");

router.get("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.profesor
    .findAll({
      attributes: ["id", "nombre", "direccion", "telefono", "mail"],
      offset: Number(req.query.offset),
      limit: Number(req.query.limit)
      
    })
    .then(profesores => res.send(profesores))
    .catch(() => res.sendStatus(500));
      }
      
      
      
  
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.post("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.profesor
    .create({ nombre: req.body.nombre, direccion: req.body.direccion, telefono: req.body.telefono,
        mail: req.body.mail })
    .then(profesor => res.status(201).send({ id: profesor.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: existe otra profesor con el mismo nombre')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
      }
      
      
      
  
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

const findProfesor = (id, { onSuccess, onNotFound, onError }) => {
  models.profesor
    .findOne({
      attributes: ["id", "nombre", "direccion", "telefono", "mail"],
      where: { id }
    })
    .then(profesor => (profesor ? onSuccess(profesor) : onNotFound()))
    .catch(() => onError());
  
};

router.get("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        findProfesor(req.params.id, {
          onSuccess: profesor => res.send(profesor),
          onNotFound: () => res.sendStatus(404),
          onError: () => res.sendStatus(500)
        });
      }
      
      
      
  
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.put("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = profesor =>
        profesor
          .update({ nombre: req.body.nombre, direccion: req.body.direccion, telefono: req.body.telefono,
            mail: req.body.mail}, { fields: ["nombre", "direccion", "telefono", "mail"] })
          .then(() => res.sendStatus(200))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: existe otro profesor con el mismo nombre')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
        findProfesor(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
           
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
 
});

router.delete("/:id", async (req, res) => {

  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = profesor =>
    profesor
      .destroy()
      .then(() => res.sendStatus(200))
      .catch(() => res.sendStatus(500));
  findProfesor(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
      }
           
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

module.exports = router;