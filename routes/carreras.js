var express = require("express");
var router = express.Router();
var models = require("../models");

router.get("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.carrera
        .findAll({
          attributes: ["id", "nombre"],
          offset: Number(req.query.offset),
          limit: Number(req.query.limit)
        })
        .then(carreras => res.send(carreras))
        .catch(() => res.sendStatus(500));
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.post("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.carrera
    .create({ nombre: req.body.nombre })
    .then(carrera => res.status(201).send({ id: carrera.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

const findCarrera = (id, { onSuccess, onNotFound, onError }) => {
  models.carrera
    .findOne({
      attributes: ["id", "nombre"],
      where: { id }
    })
    .then(carrera => (carrera ? onSuccess(carrera) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        findCarrera(req.params.id, {
          onSuccess: carrera => res.send(carrera),
          onNotFound: () => res.sendStatus(404),
          onError: () => res.sendStatus(500)
        });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
 
});

router.put("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = carrera =>
    carrera
      .update({ nombre: req.body.nombre }, { fields: ["nombre"] })
      .then(() => res.sendStatus(200))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otra carrera con el mismo nombre')
        }
        else {
          console.log(`Error al intentar actualizar la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
    findCarrera(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
 
});

router.delete("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = carrera =>
        carrera
          .destroy()
          .then(() => res.sendStatus(200))
          .catch(() => res.sendStatus(500));
      findCarrera(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

module.exports = router;
