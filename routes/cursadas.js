var express = require("express");
var router = express.Router();
var models = require("../models");

router.get("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.cursada
        .findAll({
            attributes: ["id", "parcial1", "parcial2", "final"],
            include:[{as:'Alumno-Relacionado', model:models.alumno, attributes: ["id", "nombre", "direccion", "telefono", "mail"]},
            {as:'Curso-Relacionado', model:models.curso, attributes: ["id", "horario"],
          include:[{as:'Materia-Relacionada', model:models.materia, attributes: ["id", "nombre"]},
          {as:'Profesor-Relacionado', model:models.profesor, attributes: ["id", "nombre", "direccion", "telefono", "mail"]}
      
          ]}],
           
          })
    .then(cursadas => res.send(cursadas))
    .catch(() => res.sendStatus(500));
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.post("/", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        models.cursada
    .create({ parcial1: req.body.parcial1, parcial2: req.body.parcial2, final: req.body.final,
     id_curso: req.body.id_curso, id_alumno: req.body.id_alumno })
    .then(cursada => res.status(201).send({ id: cursada.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: error inesperado')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

const findCursada = (id, { onSuccess, onNotFound, onError }) => {
  
  models.cursada
    .findOne({
      attributes: ["id", "parcial1", "parcial2", "final"],
      include:[{as:'Alumno-Relacionado', model:models.alumno, attributes: ["id", "nombre", "direccion", "telefono", "mail"]},
      {as:'Curso-Relacionado', model:models.curso, attributes: ["id", "horario"],
    include:[{as:'Materia-Relacionada', model:models.materia, attributes: ["id", "nombre"]},
    {as:'Profesor-Relacionado', model:models.profesor, attributes: ["id", "nombre", "direccion", "telefono", "mail"]}

    ]}],
      where: { id }
    })
    .then(cursada => (cursada ? onSuccess(cursada) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        findCursada(req.params.id, {
          onSuccess: cursada => res.send(cursada),
          onNotFound: () => res.sendStatus(404),
          onError: () => res.sendStatus(500)
        });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.put("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = cursada =>
        cursada
          .update({ parcial1: req.body.parcial1, parcial2: req.body.parcial2, final: req.body.final,
            id_curso: req.body.id_curso, id_alumno: req.body.id_alumno }, { fields: ["parcial1", "parcial2", "final", "id_curso", "id_alumno"] })
          .then(() => res.sendStatus(200))
          .catch(error => {
            if (error == "SequelizeUniqueConstraintError: Validation error") {
              res.status(400).send('Bad request: error inesperado')
            }
            else {
              console.log(`Error al intentar actualizar la base de datos: ${error}`)
              res.sendStatus(500)
            }
          });
        findCursada(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

router.delete("/:id", async (req, res) => {
  const { email, pass } = req.headers;
    try {
      //AUTHENTICO AL USUARIO PARA VER QUE EXISTA Y LAS CREDENCIALES SEAN CORRECTAS
      let user = await models.user.authenticate(email, pass);
      if(user){
        const onSuccess = cursada =>
        cursada
          .destroy()
          .then(() => res.sendStatus(200))
          .catch(() => res.sendStatus(500));
      findCursada(req.params.id, {
        onSuccess,
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
      }
    } catch (err) {
      return res.status(400).send('invalid email or pass');
    }
  
});

module.exports = router;