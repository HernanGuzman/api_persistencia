'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('cursadas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      parcial1: {
        type: Sequelize.DOUBLE
      },
      parcial2: {
        type: Sequelize.DOUBLE
      },
      final: {
        type: Sequelize.DOUBLE
      },
      id_alumno: {
        type: Sequelize.INTEGER
      },
      id_curso: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('cursadas');
  }
};