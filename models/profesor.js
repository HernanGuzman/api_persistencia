'use strict';
module.exports = (sequelize, DataTypes) => {
  const profesor = sequelize.define('profesor', {
    nombre: DataTypes.STRING,
    direccion: DataTypes.STRING,
    telefono: DataTypes.STRING,
    mail: DataTypes.STRING
  }, {});
  profesor.associate = function(models) {
    // associations can be defined here
  };
  return profesor;
};