'use strict';
module.exports = (sequelize, DataTypes) => {
  const cursada = sequelize.define('cursada', {
    parcial1: DataTypes.DOUBLE,
    parcial2: DataTypes.DOUBLE,
    final: DataTypes.DOUBLE,
    id_alumno: DataTypes.INTEGER,
    id_curso: DataTypes.INTEGER
  }, {});
  cursada.associate = function(models) {
    // associations can be defined here
    cursada.belongsTo(models.alumno
      ,{
        as : 'Alumno-Relacionado',
        foreignKey: 'id_alumno'}),
        cursada.belongsTo(models.curso
          ,{
            as : 'Curso-Relacionado',
            foreignKey: 'id_curso'})
  };
  return cursada;
};