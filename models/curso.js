'use strict';
module.exports = (sequelize, DataTypes) => {
  const curso = sequelize.define('curso', {
    horario: DataTypes.STRING,
    id_materia: DataTypes.INTEGER,
    id_profesor: DataTypes.INTEGER
  }, {});
  curso.associate = function(models) {
    // associations can be defined here
    curso.belongsTo(models.materia
      ,{
        as : 'Materia-Relacionada',
        foreignKey: 'id_materia'}),
    curso.belongsTo(models.profesor
          ,{
            as : 'Profesor-Relacionado',
            foreignKey: 'id_profesor'})

  };
  return curso;
};