const bcrypt = require("bcrypt");
'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    nombre: DataTypes.STRING,
    email: DataTypes.STRING,
    pass: DataTypes.STRING
  }, {});
  
  
  
  
  // e.g. User.authenticate('hernan@mail.com', 'password1234')
  user.authenticate = async function(email, pass) {
    // Si el mail o el password no se envia
    // envia un bad request con error 404
    if (!email || !pass) {
      return res.status(400).send(
        'Request missing email or pass param'
      );
    }
    //Busco el usuario con el mail recibido
    const usu = await user.findOne({ where: { email } });
    //Comparo el password con el password guardado en la BD
     if (bcrypt.compareSync(pass, usu.pass)) {
      //Si el password es correcto devuelve el usuario
      return usu;
    }

    throw new Error('invalid password');
  }

  

  
  return user;
};